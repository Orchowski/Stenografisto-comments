using System;
using System.ComponentModel;
using Microsoft.AspNetCore.Mvc.Internal;
using Newtonsoft.Json;

namespace Prater_api.Views
{
    public class CommentView
    {
        public string Id { get; set; }
        public string Message { get; set; }
        public string Author { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? UpdateDate { get; set; }
        public string RelatedResourceId { get; set; }
        public RelatedResources RelatedResources { get; set; }
        public int SubCommentCount { get; set; }
    }
}