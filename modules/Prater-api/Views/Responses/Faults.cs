namespace Prater_api.Views.Responses
{
    public enum Faults {
        VALIDATION, ACCESS_DENIED, NO_DATA
    }
}