namespace Prater_api.Views.Responses
{
    public interface IFault
    {
            Faults Type { get; }

            string Message { get; }

            string Details { get; }
    }
}