using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace Prater_api.Views.Responses
{
    public class ApiResponse<T> : JsonResult where T : IResponseContent
    {
        private readonly HttpStatusCode _httpStatus;

        public ApiResponse(SystemResponse<T> data, HttpStatusCode httpStatus) : base(data)
        {
            _httpStatus = httpStatus;
        }

        public ApiResponse(SystemResponse<T> data) : base(data)
        {
            _httpStatus = HttpStatusCode.OK;
        }

        public override Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = (int) _httpStatus;
            return base.ExecuteResultAsync(context);
        }
    }
}