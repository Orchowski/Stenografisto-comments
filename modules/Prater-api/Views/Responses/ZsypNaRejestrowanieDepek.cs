using Microsoft.Extensions.DependencyInjection;
using Prater_api.Commands;

namespace Prater_api.Views.Responses
{
    public static class ZsypNaRejestrowanieDepek
    {
        public static void AddAppDependencies(this IServiceCollection services)
        {
            services.AddScoped<CommentCommand>();
            services.AddScoped<CreateCommentCommand>();
            services.AddScoped<DeleteCommentCommand>();
            services.AddScoped<ResourceCommentsCommand>();
            services.AddScoped<UpdateCommentCommand>();
        }
    }
}