namespace Prater_api.Views.Responses
{
    public interface IValidationError
    {
        string Field { get; }
        string Detail { get; }
    }
}