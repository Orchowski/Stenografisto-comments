namespace Prater_api.Views.Responses
{
    public class ValidationError : IValidationError
    {
        public string Field { get; }
        public string Detail { get; }

        public ValidationError(string field, string detail)
        {
            Field = field;
            Detail = detail;
        }
    }
}