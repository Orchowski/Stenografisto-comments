using System;

namespace Prater_api.Views.Responses
{
    public class Fault : IFault
    {
        public Fault(Faults type, string message, string details)
        {
            Type = type;
            Message = message;
            Details = details;
        }

        public Faults Type { get; }
        public string Message { get; }
        public string Details { get; }
    }
}