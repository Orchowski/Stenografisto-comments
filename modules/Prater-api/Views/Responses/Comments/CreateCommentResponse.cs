using System.Collections.Generic;

namespace Prater_api.Views.Responses.Comments
{
    public class CreateCommentResponse : IResponseContent
    {
        public string CreatedCommentId { get; set; }
        public string Message { get; set; }
    }
}