using System.Collections.Generic;

namespace Prater_api.Views.Responses.Comments
{
    public class CommentsListResponse : IResponseContent
    {
        public IEnumerable<CommentView> Comments { get; set; }
    }
}