using System.Collections.Generic;

namespace Prater_api.Views.Responses.Comments
{
    public class SingleCommentResponse : IResponseContent
    {
        public CommentView Comment { get; set; }
    }
}