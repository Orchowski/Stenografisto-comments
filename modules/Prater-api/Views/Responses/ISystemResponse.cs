using System.Collections.Generic;

namespace Prater_api.Views.Responses
{
    public interface ISystemResponse
    {
         IFault Fault { get; }

         IEnumerable<ValidationError> ValidationErrors { get; }
    }
}