using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Prater_api.Views;

namespace Prater_api.Storage.Entities
{
    [Table("comments")]
    public class CommentEntity
    {
        [Key]
        [Column("id")]
        [Required]
        [MaxLength(40)]
        public string Id { get; set; }

        [Required] [Column("message")] public string Message { get; set; }

        [Column("author")]
        [Required]
        [MaxLength(150)]
        public string Author { get; set; }

        [Column("created")] [Required] public DateTimeOffset CreateDate { get; set; }
        [Column("updated")] public DateTimeOffset? UpdateDate { get; set; }
        [Column("related_resource_id")]  [Required] public string RelatedResourceId { get; set; }
        [Column("related_resource_type")] [Required] public RelatedResources RelatedResourceType { get; set; }
        [Column("sub_comment_count")] [Required] public int SubCommentCount { get; set; }
    }
}