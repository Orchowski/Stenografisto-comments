using System;
using System.Collections.Generic;
using System.Linq;
using Prater_api.Storage.Entities;
using Prater_api.Views;

namespace Prater_api.Storage.Mappers
{
    public class CommentsMapper
    {
        public CommentView Map(CommentEntity e)
        {
            CommentView result = null;
            if (e != null)
            {
                result = new CommentView
                {
                    Id = e.Id,
                    Author = e.Author,
                    RelatedResources = e.RelatedResourceType,
                    Message = e.Message,
                    CreateDate = e.CreateDate,
                    UpdateDate = e.UpdateDate,
                    RelatedResourceId = e.RelatedResourceId,
                    SubCommentCount = e.SubCommentCount
                };
            }

            return result;
        }

        public CommentEntity Map(CommentView v)
        {
            CommentEntity result = null;
            if (v != null)
            {
                result = new CommentEntity
                {
                    Id = v.Id,
                    Author = v.Author,
                    RelatedResourceType = v.RelatedResources,
                    Message = v.Message,
                    CreateDate = v.CreateDate,
                    UpdateDate = v.UpdateDate,
                    RelatedResourceId = v.RelatedResourceId,
                    SubCommentCount = v.SubCommentCount
                };
            }

            return result;
        }
        
        public List<CommentView> Map(List<CommentEntity> list)
        {
            return list.Select(Map).ToList();
        }
        
        public List<CommentEntity> Map(List<CommentView> list)
        {
            return list.Select(Map).ToList();
        }
    }
}