using Microsoft.EntityFrameworkCore;
using Prater_api.Storage.Entities;

namespace Prater_api.Storage
{
    public class StorageContext : DbContext
    {
        public virtual DbSet<CommentEntity> Comments { get; set; }

        public StorageContext(DbContextOptions<StorageContext> options) : base(options)
        {
        }
    }
}