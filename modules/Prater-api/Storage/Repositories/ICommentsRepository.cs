using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Prater_api.Views;

namespace Prater_api.Storage.Repositories
{
    public interface ICommentsRepository
    {
        Task<CommentView> GetById(Guid id);
        Task<List<CommentView>> GetCommentsForResource(string resourceId, RelatedResources type);
        Task<List<CommentView>> GetAuthorComments(string author);
        Task Create(CommentView commentView);
        Task<CommentView>  Update(CommentView commentView);
        Task Delete(Guid id);
        Task IncrementCount(string id);
    }
}