using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Prater_api.Storage.Entities;
using Prater_api.Storage.Mappers;
using Prater_api.Views;

namespace Prater_api.Storage.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly StorageContext _context;
        private readonly CommentsMapper _mappers = new CommentsMapper();

        public CommentsRepository(StorageContext context)
        {
            _context = context;
        }


        public async Task<CommentView> GetById(Guid id)
        {
            var dbId = id.ToString();
            var entityFound = await _context.Comments.FirstOrDefaultAsync(c => c.Id == dbId);
            return _mappers.Map(entityFound);
        }

        public async Task<List<CommentView>> GetCommentsForResource(string resourceId, RelatedResources type)
        {
            var entitiesFound = await _context.Comments.Where(c => c.RelatedResourceId == resourceId)
                .Where(c => c.RelatedResourceType == type).ToListAsync();
            return _mappers.Map(entitiesFound);
        }

        public async Task<List<CommentView>> GetAuthorComments(string author)
        {
            var entitiesFound = await _context.Comments.Where(c => c.Author == author).ToListAsync();
            return _mappers.Map(entitiesFound);
        }

        public async Task Create(CommentView commentView)
        {
            await _context.AddAsync(_mappers.Map(commentView));
            await _context.SaveChangesAsync();
        }

        public async Task<CommentView> Update(CommentView commentView)
        {
            var dbId = commentView.Id;
            var entityFound = await _context.Comments.FirstOrDefaultAsync(c => c.Id == dbId);
            if (entityFound == null)
            {
                throw new ArgumentNullException("Cannot find comment with specified ID.");
            }
            entityFound.Message = commentView.Message;
            entityFound.UpdateDate = DateTimeOffset.Now;
            await _context.SaveChangesAsync();

            return _mappers.Map(entityFound);
        }
        
        public async Task IncrementCount(string id)
        {
            var entityFound = await _context.Comments.FirstOrDefaultAsync(c => c.Id == id);
            if (entityFound == null)
            {
                throw new ArgumentNullException("Cannot find comment with specified ID.");
            }

            entityFound.SubCommentCount++;
            
            await _context.SaveChangesAsync();
        }

        public Task Delete(Guid id)
        {
            _context.Comments.Remove(new CommentEntity
            {
                Id = id.ToString()
            });
            return Task.CompletedTask; 
        }
    }
}