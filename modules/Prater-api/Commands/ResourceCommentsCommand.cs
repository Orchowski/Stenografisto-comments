using System.Threading.Tasks;
using Prater_api.Storage.Repositories;
using Prater_api.Views;
using Prater_api.Views.Responses;
using Prater_api.Views.Responses.Comments;

namespace Prater_api.Commands
{
    public class ResourceCommentsCommand
    {
        private readonly ICommentsRepository _repo;

        public ResourceCommentsCommand(ICommentsRepository repo)
        {
            _repo = repo;
        }

        public async Task<SystemResponse<CommentsListResponse>> Execute(string id, RelatedResources type)
        {
            var comments = await _repo.GetCommentsForResource(id, type);
            return new SystemResponse<CommentsListResponse>
            {
                Data = new CommentsListResponse {Comments = comments}
            };
        }
    }
}