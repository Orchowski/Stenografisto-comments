using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Prater_api.Storage.Repositories;
using Prater_api.Views;
using Prater_api.Views.Responses;
using Prater_api.Views.Responses.Comments;

namespace Prater_api.Commands
{
    public class CommentCommand
    {
        private readonly ICommentsRepository _repo;

        public CommentCommand(ICommentsRepository repo)
        {
            _repo = repo;
        }

        public async Task<SystemResponse<SingleCommentResponse>> Execute(string id)
        {
            if (Guid.TryParse(id, out var verifiedId))
            {
                var comment = await _repo.GetById(verifiedId);
                if (comment != null)
                {
                    return new SystemResponse<SingleCommentResponse>
                    {
                        Data = new SingleCommentResponse
                        {
                            Comment = comment
                        }
                    };
                }
                
                return new SystemResponse<SingleCommentResponse>
                {
                    Fault = new Fault(Faults.NO_DATA, "Podano nieistniejacy zasob",
                        $"Komentarz o id {id} niestety nie istnieje!")
                };
            }

            return new SystemResponse<SingleCommentResponse>
            {
                Fault = new Fault(Faults.VALIDATION, "Błąd walidacji", string.Empty),
                ValidationErrors = new List<ValidationError>()
                {
                    new ValidationError(nameof(id),
                        "no więc, id musi być w formacie GUID/UID, a nie jakieś pierdoły tu wpisujesz :/")
                }
            };
        }
    }
}