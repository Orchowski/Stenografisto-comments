using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using Prater_api.Storage.Repositories;
using Prater_api.Views;
using Prater_api.Views.Responses;
using Prater_api.Views.Responses.Comments;

namespace Prater_api.Commands
{
    public class CreateCommentCommand
    {
        private readonly ICommentsRepository _repo;

        public CreateCommentCommand(ICommentsRepository repo)
        {
            _repo = repo;
        }

        public async Task<SystemResponse<CreateCommentResponse>> Execute(CommentView body)
        {
            body.Id = Guid.NewGuid().ToString();
            body.Author = "defaultUser";
            body.CreateDate = DateTimeOffset.Now;
            body.UpdateDate = null;
            body.SubCommentCount = 0;
            if (body.RelatedResources == RelatedResources.COMMENT)
            {
                return await HandleCommentRelated(body);
            }

            await _repo.Create(body);
            return ToCreatedSuccessfully(body);
        }

        private static SystemResponse<CreateCommentResponse> ToCreatedSuccessfully(CommentView body)
        {
            return new SystemResponse<CreateCommentResponse>
            {
                Data = new CreateCommentResponse
                {
                    Message = "^^",
                    CreatedCommentId = body.Id
                }
            };
        }

        private async Task<SystemResponse<CreateCommentResponse>> HandleCommentRelated(CommentView body)
        {
            if (!Guid.TryParse(body.RelatedResourceId, out Guid resourceId))
            {
                return ToRelatedResourceWrongFormatError();
            }

            var related = await _repo.GetById(resourceId);

            if (related == null)
            {
                return ToRelatedResourceNotFoundError();
            }

            if (related.RelatedResources != RelatedResources.COMMENT)
            {
                return await CreateRelatedComment(body, related);
            }

            return UnsupportedNesting();
        }

        private SystemResponse<CreateCommentResponse> ToRelatedResourceWrongFormatError()
        {
            return new SystemResponse<CreateCommentResponse>
            {
                Fault = new Fault(Faults.VALIDATION, "Błąd walidacji", string.Empty),
                ValidationErrors = new List<ValidationError>()
                {
                    new ValidationError(nameof(CommentView.RelatedResourceId),
                        "no więc, id musi być w formacie GUID/UID, a nie jakieś pierdoły tu wpisujesz :/")
                }
            };
        }


        private SystemResponse<CreateCommentResponse> ToRelatedResourceNotFoundError()
        {
            return new SystemResponse<CreateCommentResponse>
            {
                Fault = new Fault(Faults.NO_DATA, "Błąd walidacji", "Zależność nie istnieje")
            };
        }

        private async Task<SystemResponse<CreateCommentResponse>> CreateRelatedComment(CommentView body,
            CommentView related)
        {
            await _repo.Create(body);
            await _repo.IncrementCount(related.Id);
            return new SystemResponse<CreateCommentResponse>
            {
                Data = new CreateCommentResponse
                {
                    Message = "^^",
                    CreatedCommentId = body.Id
                }
            };
        }

        private SystemResponse<CreateCommentResponse> UnsupportedNesting()
        {
            return new SystemResponse<CreateCommentResponse>
            {
                Fault = new Fault(Faults.VALIDATION, "Błąd walidacji",
                    "Tylko jedno zagnieżdżenie jest dozwolone ciołksie :P")
            };
        }
    }
}