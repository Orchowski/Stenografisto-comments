using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Prater_api.Storage.Repositories;
using Prater_api.Views;
using Prater_api.Views.Responses;
using Prater_api.Views.Responses.Comments;

namespace Prater_api.Commands
{
    public class DeleteCommentCommand
    {
        private readonly ICommentsRepository _repo;

        public DeleteCommentCommand(ICommentsRepository repo)
        {
            _repo = repo;
        }

        public async Task<SystemResponse<DeleteCommentResponse>> Execute(string id)
        {
            if (Guid.TryParse(id, out var verifiedId))
            {
                _repo.Delete(verifiedId);
                return new SystemResponse<DeleteCommentResponse>();
            }

            return new SystemResponse<DeleteCommentResponse>
            {
                Fault = new Fault(Faults.VALIDATION, "Błąd walidacji", string.Empty),
                ValidationErrors = new List<ValidationError>()
                {
                    new ValidationError(nameof(id),
                        "no więc, id musi być w formacie GUID/UID, a nie jakieś pierdoły tu wpisujesz :/")
                }
            };
        }
    }
}