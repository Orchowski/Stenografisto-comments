using System;
using System.Threading.Tasks;
using Prater_api.Storage.Repositories;
using Prater_api.Views;
using Prater_api.Views.Responses;
using Prater_api.Views.Responses.Comments;

namespace Prater_api.Commands
{
    public class UpdateCommentCommand
    {
        private readonly ICommentsRepository _repo;

        public UpdateCommentCommand(ICommentsRepository repo)
        {
            _repo = repo;
        }

        public async Task<SystemResponse<UpdateCommentResponse>> Execute(CommentView body)
        {
            try
            {
                await _repo.Update(body);
            }
            catch (ArgumentNullException e)
            {
                return new SystemResponse<UpdateCommentResponse>
                {
                    Fault = new Fault(Faults.NO_DATA, "error occured", e.Message)
                };
            }

            return new SystemResponse<UpdateCommentResponse>();
        }
    }
}