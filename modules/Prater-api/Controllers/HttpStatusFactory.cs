using System.Collections.Generic;
using System.IO.Enumeration;
using System.Net;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Prater_api.Views.Responses;
using Remotion.Linq.Clauses;

namespace Prater_api.Controllers
{
    public static class HttpStatusFactory
    {
        private readonly static Dictionary<Faults, HttpStatusCode> _codesMappings =
            new Dictionary<Faults, HttpStatusCode>()
            {
                {Faults.NO_DATA, HttpStatusCode.NotFound},
                {Faults.VALIDATION, HttpStatusCode.BadRequest},
                {Faults.ACCESS_DENIED, HttpStatusCode.Forbidden}
            };

        public static HttpStatusCode ToStatus<T>(this SystemResponse<T> x) where T : IResponseContent
        {
            if (x.Fault != null)
            {
                return _codesMappings[x.Fault.Type];
            }

            if (x.ValidationErrors != null)
            {
                return _codesMappings[Faults.VALIDATION];
            }

            return HttpStatusCode.OK;
        }

        public static HttpStatusCode ToStatusWithDefaultSucceeded<T>(this SystemResponse<T> x,
            HttpStatusCode defaultStatus) where T : IResponseContent
        {
            if (x.Fault != null)
            {
                return _codesMappings[x.Fault.Type];
            }

            if (x.ValidationErrors != null)
            {
                return _codesMappings[Faults.VALIDATION];
            }

            return defaultStatus;
        }
    }
}