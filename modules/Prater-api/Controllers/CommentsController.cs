﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prater_api.Commands;
using Prater_api.Storage.Entities;
using Prater_api.Storage.Mappers;
using Prater_api.Storage.Repositories;
using Prater_api.Views;
using Prater_api.Views.Responses;
using Prater_api.Views.Responses.Comments;

namespace Prater_api.Controllers
{
    [Route("comments")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ResourceCommentsCommand _resourceComments;
        private readonly CommentCommand _commentCommand;
        private readonly CreateCommentCommand _createComment;
        private readonly DeleteCommentCommand _deleteComment;
        private readonly UpdateCommentCommand _updateComment;

        public CommentsController(
            ResourceCommentsCommand resourceComments,
            CommentCommand commentCommand,
            CreateCommentCommand createComment,
            UpdateCommentCommand updateComment,
            DeleteCommentCommand deleteComment)
        {
            _resourceComments = resourceComments;
            _commentCommand = commentCommand;
            _createComment = createComment;
            _updateComment = updateComment;
            _deleteComment = deleteComment;
        }

        [HttpGet("hello_everyone")]
        public IActionResult HelloMessage(){
            return new JsonResult(new{
                Author = "Aleksandro! El cupakabra",
                Message = "yo! i'm alive!",
                Additional = "I'm propably running on Google Cloud's Kubernetes. So exciting! But you, know... It is .NET application... propably i'm outdated too :(",
            });
        }


        [HttpGet("{id}/{type}")]
        public async Task<ApiResponse<CommentsListResponse>> GetForResource(string id, RelatedResources type)
        {
            var result = await _resourceComments.Execute(id, type);
            return new ApiResponse<CommentsListResponse>(result, result.ToStatus());
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse<SingleCommentResponse>> Get(string id)
        {
            var result = await _commentCommand.Execute(id);
            return new ApiResponse<SingleCommentResponse>(result, result.ToStatus());
        }


        [HttpPost]
        public async Task<ApiResponse<CreateCommentResponse>> Post([FromBody] CommentView comment)
        {
            var result = await _createComment.Execute(comment);
            return new ApiResponse<CreateCommentResponse>(result, result.ToStatus());
        }

        [HttpPut("{id}")]
        public async Task<ApiResponse<UpdateCommentResponse>> Put(string id, [FromBody] CommentView comment)
        {
            comment.Id = id;
            var result = await _updateComment.Execute(comment);
            return new ApiResponse<UpdateCommentResponse>(result, result.ToStatus());
        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse<DeleteCommentResponse>> Delete(string id)
        {
            var result = await _deleteComment.Execute(id);
            return new ApiResponse<DeleteCommentResponse>(result, result.ToStatus());
        }
    }
}