﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Prater_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((b) =>
                {
                    b.AddJsonFile(Environment.CurrentDirectory + "/Config/config.json");
                    b.AddJsonFile(Environment.CurrentDirectory + "/Config/appsettings.Development.json");
                    b.AddJsonFile(Environment.CurrentDirectory + "/Config/appsettings.json");
                })
                .UseWebRoot("wwwroot")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>();
    }
}