﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Praterapi.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "comments",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 40, nullable: false),
                    message = table.Column<string>(nullable: false),
                    author = table.Column<string>(maxLength: 150, nullable: false),
                    created = table.Column<DateTimeOffset>(nullable: false),
                    updated = table.Column<DateTimeOffset>(nullable: true),
                    related_resource_id = table.Column<string>(nullable: false),
                    related_resource_type = table.Column<int>(nullable: false),
                    sub_comment_count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_comments", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "comments");
        }
    }
}
