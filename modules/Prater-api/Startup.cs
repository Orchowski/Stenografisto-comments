﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Prater_api.Storage;
using Prater_api.Storage.Repositories;
using Prater_api.Views.Responses;
using Swashbuckle.AspNetCore.Swagger;

namespace Prater_api
{
    public class Startup
    {
        private string version;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            version = configuration.GetValue<string>("Version");
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetValue<string>("connectionString");
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateFormatString = "yyyy-MM-ddThh:mm:ssZ";
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                });
            services.AddSwaggerGen(c => { c.SwaggerDoc(version, new Info {Title = "Prater", Version = version}); });
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<StorageContext>(options => options.UseNpgsql(connectionString));
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            services.AddAppDependencies();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseStaticFiles();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"Prater version {version}");
                c.RoutePrefix = string.Empty;
            });
            app.UseMvc();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<StorageContext>();
                context.Database.Migrate();
            }
        }
    }
}