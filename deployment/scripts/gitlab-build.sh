#!/bin/bash

#reads actual version from git tags
APP_VERSION=`git describe --tags --abbrev=0`


echo "The version = $APP_VERSION"
echo "And tags:"
git describe --tags


#build package
dotnet restore
dotnet publish -c Release

#creating artifacts

mv ./modules/Prater-api/bin/Release/netcoreapp2.1/publish  ./artifacts/
mv Dockerfile ./artifacts/Dockerfile

#sets application versions

sed -i -- 's/{APP_VERSION}/'"$APP_VERSION"'/g' ./artifacts/publish/Config/appsettings.json

echo $APP_VERSION > ./artifacts/APP_VERSION.info