FROM microsoft/dotnet:2.1-sdk
WORKDIR /opt/app
MAINTAINER Aleksander Orchowski - www.orchowskia.com
COPY ./artifacts/publish ./

CMD dotnet Prater-api.dll
